// truc standard de c++
// Lien du contenu http://www.cplusplus.com/reference/iostream/
#include <iostream>
// utiliser pour la fonction rand, pour initialiser la suite cahotique en fonction du temps
// Se base sur le temps en seconde // Lien http://www.cplusplus.com/reference/ctime/time/
#include <time.h>
// truc standard de c
// Lien des contenus http://www.cplusplus.com/reference/cstdio/
// Lien des contenues http://www.cplusplus.com/reference/cstdlib/
#include <stdio.h>
#include <stdlib.h>
// The header <algorithm> defines a collection of functions especially designed to be used on ranges of elements.
// Lien pour voir ce que ce header contient http://www.cplusplus.com/reference/algorithm/
#include <algorithm>
// Biblio pour utiliser des Bool�ens en C, non pr�sent de base
#include <stdbool.h>

// Pour utiliser un namespace et �viter de se taper les std:: devant les truc comme cout ==> std::cout
using namespace std;

// Ceci est un prototype de ma fonction afficher hello world
// l va servir � indiquer au programme que une fonction existe en dessous de main
void DisplayHelloWorld();
float ReturnPyValue(int NbDecimal = 4);
void NomGeneriqueDeFonction(int * TabPointer, int SizeTab);
void DisplayStruct(struct TemplateCharacter Player);

// D�clarer une structure

struct TemplateCharacter{
    string name;
    int Victories;
    int Pv;
}Pile, Face;

int main()
{
    /*
        #### Afficher un truc ####
    */
    // C
    int intValue = 15;
    printf(" Je suis un entier %d ", intValue);

    //C++
    // std::endl
    cout << " Je suis un entier " << intValue << endl;

    /*
        #### R�cup�rer une ent�e ####
    */
    //C
    int intValue2 =0;
    scanf("Entrer un entier :%d", &intValue2);
    // Toujours mettre dans un pointeur sinon �a plante

    //C++
    int intValue3 = 0;
    cin >> intValue3;

    /*
        #### Quelque �gal ####
    */
    // =    Affecte une valeur
    // ==   Compare
    // ===  Comparaison stricte, compare la valeur et le type de la variable

    /*
        #### L'incr�mentation, d�-cr�mentation ####
    */
    int i = 0;

    i = i + 1;
    i += 1;
    i++;

    i = i -1;
    i-= 1;
    i--;

    /*
        #### Les conditions ####
    */
    //C
    int x = 0;
    if (x > 0){
        cout << "x is positive";
    }
    else if (x < 0){
        cout << "x is negative";
    }
    else{
        cout << "x is 0";
    }

    //C++
    if (x > 0)
        cout << "x is positive";
    else if (x < 0)
        cout << "x is negative";
    else
        cout << "x is 0";

    /*
        #### Les boucles ####
    */

    //WHILE
    bool something = false;
    while(something == true){
        cout << "Hello world" << endl;
    }

    //DO WHILE
    int intValue4;
    do {
        cout << "Enter number: ";
        cin >> intValue4;
        cout << "You entered: " << intValue4 << endl;
    } while (something == true);

    //FOR
    int n;
    for (n=0; n<11 ; n++) {
        cout << n << endl;
    }

    /*
        #### Les fonctions + SWITCH ####
    */
    // Appel de la fonction, sans arguments
    DisplayHelloWorld();

    float MyPi;
    MyPi = ReturnPyValue(2);
    cout <<  MyPi << endl;

    /*
        #### G�rer le random ####
    */
    // bien inclure les biblio C
    srand(time(NULL));                      // init en d�but de mai avec cette ligne
    int VariableGenerique = (rand()%3)+1;   // Remplis la var avec une valeur de (0 � 2)+1 donc de 1 � 3

    /*
        #### Les tableaux ####
    */
    // Cr�er un tableau
    int Table[10];                          // va cr�er un tableau de 10 case allant de 0 � 9

    // Parcourir  un tableau
    int k = 0;
    for(k=0; k <10; k++){
        Table[k] = 0;                                                   // Remplis de 0 le tableau
        cout << "Ligne " << k << " contient " << Table[k] << endl;      // Affiche l'index du tableau + son contenu
    }

    // Envoyer r�cup un tableau en arguments de fonctions
    NomGeneriqueDeFonction(Table, 10);                                  // On met la table plus sa taille en arguments

    /*
        #### Les structures ####
    */
    //D�claration en haut du main
    // Utiliser une structure
    Pile.name = "Hello";
    Face.Pv = 3;

    // Passer une structure en arguments de fonction
    struct TemplateCharacter Player;
    DisplayStruct(Player);

    // Parcourir une structure


    return 0;
}

void DisplayHelloWorld(){
    cout << "Hello world" << endl;
}

// la fonction aura un seul argument optionnel
float ReturnPyValue(int NbDecimal){
    // ici code pour la fonction,
    switch(NbDecimal){
    case 1:
        return 3.1;
    break;
    case 2:
        return 3.14;
    break;
    case 3:
        return 3.141;
    break;
    case 5:
        return 3.14159;
    break;
    default:
        return 3.1415;
    break;
    }
}

// Petite fonction pour afficher le contenu d'un tableau
void NomGeneriqueDeFonction(int * TabPointer, int SizeTab)      // On r�cup�re le tableau sous forme de pointeur vers la premi�re valeurs du tableau
{
    // Afficher le tableau
    int i;
    for (i = 0 ; i < SizeTab ; i++){
        cout << TabPointer[i]  << endl;
    }
}

void DisplayStruct(struct TemplateCharacter Player)             // Le r�cup�rer comme si on la d�clare
{
    cout << Player.name << endl;                                // Puis l'afficher
}
